package com.example.mpay;

public class Customer {
    private int id;
    private  int customerid;
    private  int vat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerid() {
        return customerid;
    }

    public void setCustomerid(int customerid) {
        this.customerid = customerid;
    }
    public int getvat() {
        return vat;
    }

    public void setvat(int vat) {
        this.vat = vat;
    }
}
